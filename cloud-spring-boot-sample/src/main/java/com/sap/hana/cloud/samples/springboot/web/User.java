package com.sap.hana.cloud.samples.springboot.web;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author shivareddy.busireddy
 *
 */

@Entity
@Table(name = "\"SHIVAREDDY\".\"USER\"")
@NamedQueries({
		@NamedQuery(name = "fetchByNamePwd", query = "select e from User e where e.userName=:name and e.password=:pwd"),
		@NamedQuery(name = "fetchAll", query = "select u from User u") })
public class User implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	private int id;
	@Column(name = "USERNAME")
	private String userName;
	@Column(name = "PASSWORD")
	private String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
