package com.sap.hana.cloud.samples.springboot.web;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class LoginController {

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger log = LoggerFactory.getLogger(LoginController.class);

	@RequestMapping("/getUser")
	@ResponseBody
	public List<User> getAllUser() {

		String qury = "" + "select " + "\"ID\", " + "\"USERNAME\", " + "\"PASSWORD\"  from \"SHIVAREDDY\".\"USER\"";
		@SuppressWarnings("unchecked")
		List<User> resultList = entityManager.createNativeQuery(qury, User.class).getResultList();
		this.log.debug("getAllUser" + resultList.size());
		for (User userlist : resultList) {
			System.out.println("username: " + userlist.getUserName() + " :password: " + userlist.getPassword());

		}

		return resultList;
	}

}
